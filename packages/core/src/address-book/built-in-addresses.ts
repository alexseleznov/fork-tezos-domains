export const BuiltInAddresses = {
    mainnet: {
        'nameRegistry:set_child_record': {
            address: 'KT1QHLk1EMUA8BPH3FvRUeUmbTspmAhb7kpd',
        },
        'nameRegistry:update_record': {
            address: 'KT1H1MqmUM4aK9i1833EBmYCCEfkbt6ZdSBc',
        },
        'nameRegistry:claim_reverse_record': {
            address: 'KT1TnTr6b2YxSx2xUQ8Vz3MoWy771ta66yGx',
        },
        'nameRegistry:update_reverse_record': {
            address: 'KT1J9VpjiH5cmcsskNb8gEXpBtjD4zrAx4Vo',
        },
        nameRegistry: {
            address: 'KT1F7JKNqwaoLzRsMio1MQC7zv3jG9dHcDdJ',
            resolveProxyContract: true,
        },
        'tldRegistrar:tez:commit': {
            address: 'KT1P8n2qzJjwMPbHJfi4o8xu6Pe3gaU3u2A3',
        },
        'tldRegistrar:tez': {
            address: 'KT1P8n2qzJjwMPbHJfi4o8xu6Pe3gaU3u2A3',
            resolveProxyContract: true,
        },
        'tldRegistrar:tez:buy': {
            address: 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr',
        },
        'tldRegistrar:tez:renew': {
            address: 'KT1EVYBj3f1rZHNeUtq4ZvVxPTs77wuHwARU',
        },
        'tldRegistrar:tez:bid': {
            address: 'KT1CaSP4dn8wasbMsfdtGiCPgYFW7bvnPRRT',
        },
        'tldRegistrar:tez:withdraw': {
            address: 'KT1CfuAbJQbAGYcjKfvEvbtNUx45LY5hfTVR',
        },
        'tldRegistrar:tez:settle': {
            address: 'KT1MeFfi4TzSCc8CF9j3qq5mecTPdc6YVUPp',
        },
        oracleRegistrar: {
            address: 'KT1MMkh1VMT8d5BVuBP2whf7RXCWeMdUfkXu',
        },
    },
    custom: null,
    ghostnet: {
        'nameRegistry:set_child_record': {
            address: 'KT1HpddfW7rX5aT2cTdsDaQZnH46bU7jQSTU',
        },
        'nameRegistry:update_record': {
            address: 'KT1Ln4t64RdCG1bK8zkH6Xi4nNQVxz7qNgyj',
        },
        'nameRegistry:claim_reverse_record': {
            address: 'KT1H19ouy5QwDBchKXcUw1QRFs5ZYyx1ezEJ',
        },
        'nameRegistry:update_reverse_record': {
            address: 'KT1HDUc2xtPHqWQcjE1WuinTTHajXQN3asdk',
        },
        nameRegistry: {
            address: 'KT1B3j3At2XMF5P8bVoPD2WeJbZ9eaPiu3pD',
            resolveProxyContract: true,
        },
        'tldRegistrar:gho:commit': {
            address: 'KT1PEnPDgGKyHvaGzWj6VJJYwobToiW2frff',
        },
        'tldRegistrar:gho': {
            address: 'KT1PEnPDgGKyHvaGzWj6VJJYwobToiW2frff',
            resolveProxyContract: true,
        },
        'tldRegistrar:gho:buy': {
            address: 'KT1Ks7BBTLLjD9PsdCboCL7fYEfq8z1mEvU1',
        },
        'tldRegistrar:gho:renew': {
            address: 'KT1Bv32pdMYmBJeMa2HsyUQZiC6FNj1dX6VR',
        },
        'tldRegistrar:gho:bid': {
            address: 'KT1P3wdbusZK2sj16YXxRViezzWCPXpiE28P',
        },
        'tldRegistrar:gho:withdraw': {
            address: 'KT1C7EF4c1pnPW9qcfNRiTPj5tBFMQJtvUhq',
        },
        'tldRegistrar:gho:settle': {
            address: 'KT1DMNPg3b3fJQpjXULcXjucEXfwq3zGTKGo',
        },
        'tldRegistrar:a1:commit': {
            address: 'KT1PiEKNUgDyr4gA5Z5WE6Vi7e9MKjre6Vyy',
        },
        'tldRegistrar:a1': {
            address: 'KT1PiEKNUgDyr4gA5Z5WE6Vi7e9MKjre6Vyy',
            resolveProxyContract: true,
        },
        'tldRegistrar:a1:buy': {
            address: 'KT1C6uf5vhzjwcpNvKdQWMVg8JxFswXrurGD',
        },
        'tldRegistrar:a1:renew': {
            address: 'KT1GE4S9PqFx7RMzuA26oXRYf9iJXnMdMyDn',
        },
        'tldRegistrar:a1:bid': {
            address: 'KT1K9teWXfPPozW5VBUegPvUVgtXrnoJidV7',
        },
        'tldRegistrar:a1:withdraw': {
            address: 'KT1WMQnp2sFF7r2ECYW35prfjYFrxVczaEat',
        },
        'tldRegistrar:a1:settle': {
            address: 'KT1NBWg6RK6CH2hhvtQWofadqVeDa8MEEWmc',
        },
        'tldRegistrar:a2:commit': {
            address: 'KT1RnBSGukTTEN6NzcNVyZcArGx2VKdsL4uU',
        },
        'tldRegistrar:a2': {
            address: 'KT1RnBSGukTTEN6NzcNVyZcArGx2VKdsL4uU',
            resolveProxyContract: true,
        },
        'tldRegistrar:a2:buy': {
            address: 'KT1GUfkv3jMMuKhVwu8NFck4DHGynS3YLkn4',
        },
        'tldRegistrar:a2:renew': {
            address: 'KT1JNxJ46cJTk37Lc2fdGs7WgSwNGbimaxcr',
        },
        'tldRegistrar:a2:bid': {
            address: 'KT1VN7wyuRjXAD8W4GMCC78NAcXFv4GKY8aA',
        },
        'tldRegistrar:a2:withdraw': {
            address: 'KT1FFNsV26cY2AbtR7H6vbdAX3dQp3nk6iTj',
        },
        'tldRegistrar:a2:settle': {
            address: 'KT1WzbK971hoRBtPuifq6Dp2wsw3Fp2uodNL',
        },
        'tldRegistrar:a3:commit': {
            address: 'KT18eVEy9H2gCKZqZ3gc4ZN7EhvSPNQEwSmc',
        },
        'tldRegistrar:a3': {
            address: 'KT18eVEy9H2gCKZqZ3gc4ZN7EhvSPNQEwSmc',
            resolveProxyContract: true,
        },
        'tldRegistrar:a3:buy': {
            address: 'KT1TtPKzV94QQr4cAXV7GtiXPV95cgBhiweD',
        },
        'tldRegistrar:a3:renew': {
            address: 'KT1G3TCBEpjTR8YnZ4j3D6pehs7Xwodnng4U',
        },
        'tldRegistrar:a3:bid': {
            address: 'KT1EaiCWMt5vbqjpmjw9Wq8gxqLWFEzaPMWG',
        },
        'tldRegistrar:a3:withdraw': {
            address: 'KT1VkfPjHEjvhuinsKBEZCGgx7LMapVsVERs',
        },
        'tldRegistrar:a3:settle': {
            address: 'KT1LFMdquFkEUVu9Xf5aAtNR3BF25sprz7qh',
        },
        oracleRegistrar: {
            address: 'KT1GBZDGVJ85Sg4t3ppjGiRn4382fPeo3f3T',
        },
    },
    jakartanet: {
        'nameRegistry:set_child_record': {
            address: 'KT1Djax3uKS6JXvJguSkikmSDZDErdvYyx5P',
        },
        'nameRegistry:update_record': {
            address: 'KT1UQgpYo7M996L2xFV9TiqPVehM1TAs4uXs',
        },
        'nameRegistry:claim_reverse_record': {
            address: 'KT1C1JVViEw63rKCkxATX3jfK3UKjqF1cdxf',
        },
        'nameRegistry:update_reverse_record': {
            address: 'KT1PQ95cfVprPtDyBGCmR3vU3Un88EESTyuR',
        },
        nameRegistry: {
            address: 'KT1LZRsvqcA4cXtYKPxX9RwzT6DgdYCRuWbs',
            resolveProxyContract: true,
        },
        'tldRegistrar:jak:commit': {
            address: 'KT1EWKKYfuF32Uk136HHqdc4v195S7PieWzY',
        },
        'tldRegistrar:jak': {
            address: 'KT1EWKKYfuF32Uk136HHqdc4v195S7PieWzY',
            resolveProxyContract: true,
        },
        'tldRegistrar:jak:buy': {
            address: 'KT1Hzv2HpL57bwr14nFJerCVYiMZkQqxD8ZA',
        },
        'tldRegistrar:jak:renew': {
            address: 'KT1MPh1VZVSksqWF5mbiGbuagWmJzQShNMrd',
        },
        'tldRegistrar:jak:bid': {
            address: 'KT1LdAxSSKfLU3Deb15REh11xKhjpLCuQYGZ',
        },
        'tldRegistrar:jak:withdraw': {
            address: 'KT1SsKFchZUY8etQddpAvDZPdKKJoV5bA52Q',
        },
        'tldRegistrar:jak:settle': {
            address: 'KT1MAd6GKkVzohoeV172bYpwoyqLD5GLdmad',
        },
        'tldRegistrar:a1:commit': {
            address: 'KT1Pr7Xg9WDM4v1PzUMcZhDiUM8sZMGmz4Ut',
        },
        'tldRegistrar:a1': {
            address: 'KT1Pr7Xg9WDM4v1PzUMcZhDiUM8sZMGmz4Ut',
            resolveProxyContract: true,
        },
        'tldRegistrar:a1:buy': {
            address: 'KT1Pc374xTzGxDS8gaNobM6LmPhT4qMaUYfY',
        },
        'tldRegistrar:a1:renew': {
            address: 'KT1RY6FiUVKmuEUn4HiMKh328DqBbmJj6Av2',
        },
        'tldRegistrar:a1:bid': {
            address: 'KT1Sg2jeDWgp4tmU6pPaVq22eUnanGq6ZugY',
        },
        'tldRegistrar:a1:withdraw': {
            address: 'KT1DysPuUrtxQZtDDdfbf3WiT5wpXi8SVdio',
        },
        'tldRegistrar:a1:settle': {
            address: 'KT1PJjogYRWXDHqEFfieaM3i2UDxT4xx4fNQ',
        },
        'tldRegistrar:a2:commit': {
            address: 'KT19SfZnLUYgF3VUPNCC5ZoHALjHSXkEYcQG',
        },
        'tldRegistrar:a2': {
            address: 'KT19SfZnLUYgF3VUPNCC5ZoHALjHSXkEYcQG',
            resolveProxyContract: true,
        },
        'tldRegistrar:a2:buy': {
            address: 'KT1FARWoPasPM83WrwKbc3s3jzJFMQ6Rq2VP',
        },
        'tldRegistrar:a2:renew': {
            address: 'KT1DfTw7orWqGSUxkg7a5A7anbDgdA9JD3Uf',
        },
        'tldRegistrar:a2:bid': {
            address: 'KT19Knt9rfrxonqNTWjBNTdpgpJxAyQqJBuX',
        },
        'tldRegistrar:a2:withdraw': {
            address: 'KT1GVjXc5Y57qJFP85ptCztVMgmCRQWpBjLa',
        },
        'tldRegistrar:a2:settle': {
            address: 'KT1CmiKt5dtkdSKyoue4DCzwCzJrMi5Xr8Nk',
        },
        'tldRegistrar:a3:commit': {
            address: 'KT1UhwBDqQiRQayubhEiJPZpokpWWBafvBSi',
        },
        'tldRegistrar:a3': {
            address: 'KT1UhwBDqQiRQayubhEiJPZpokpWWBafvBSi',
            resolveProxyContract: true,
        },
        'tldRegistrar:a3:buy': {
            address: 'KT1WvXu7LQh4ZhQJjJS9iEQ4bfCY15gEHXqM',
        },
        'tldRegistrar:a3:renew': {
            address: 'KT1TbEroXmHidC4E4Dpqry42Pr2nv5XWKaHB',
        },
        'tldRegistrar:a3:bid': {
            address: 'KT1H54eCmPugM7JsGz96M2VrsMChN98wFrC4',
        },
        'tldRegistrar:a3:withdraw': {
            address: 'KT1UzYfUSTzC2PxAZ6yiA9yXWof1kSntkWLV',
        },
        'tldRegistrar:a3:settle': {
            address: 'KT1F2P5VweUnN5YNMLtpfSh2SSe61TzdaoA6',
        },
        oracleRegistrar: {
            address: 'KT1UPyEnjcPfGuCaJprna2v9YL9CyBkZfzTX',
        },
    },
    kathmandunet: {
        'nameRegistry:set_child_record': {
            address: 'KT1VJvzrekMV1vhZbG9fUVHnVU5hqCAANJcw',
        },
        'nameRegistry:update_record': {
            address: 'KT1KaFpFT5zmWhArHaFnkDTigJF1pUHhtMWN',
        },
        'nameRegistry:claim_reverse_record': {
            address: 'KT1C746oXgesnjfBrNUnnNTLrcbY4D8YTiVN',
        },
        'nameRegistry:update_reverse_record': {
            address: 'KT1KzHnkNzBzrkDGrwWThXGYbrrpkwGV5Pa3',
        },
        nameRegistry: {
            address: 'KT1KUjiThzSkuNMPtosk4MqLEivdgqC1MJat',
            resolveProxyContract: true,
        },
        'tldRegistrar:kat:commit': {
            address: 'KT1Jt2YmfQ5gNgiujUG1CR4dPPUBxfgbttQX',
        },
        'tldRegistrar:kat': {
            address: 'KT1Jt2YmfQ5gNgiujUG1CR4dPPUBxfgbttQX',
            resolveProxyContract: true,
        },
        'tldRegistrar:kat:buy': {
            address: 'KT1QBxDQ3t8ofUXD6D75213HstxFwwEQNgHc',
        },
        'tldRegistrar:kat:renew': {
            address: 'KT1FNoRTHdugUV5zj8hDHcyRWuDUUrz4fjqQ',
        },
        'tldRegistrar:kat:bid': {
            address: 'KT1GhcKyt4SLQFwEhhxompFqG138jCL98qeL',
        },
        'tldRegistrar:kat:withdraw': {
            address: 'KT1HN3WwLokPAtG98vgFuGoUVnLi2ox1b1Zi',
        },
        'tldRegistrar:kat:settle': {
            address: 'KT1EFgjaG4L6baz73iBFP3bjwHiHJk8AgMt1',
        },
        'tldRegistrar:a1:commit': {
            address: 'KT1GRfsS8qeW3jWgdpYkqwbqmiD768EN13Vw',
        },
        'tldRegistrar:a1': {
            address: 'KT1GRfsS8qeW3jWgdpYkqwbqmiD768EN13Vw',
            resolveProxyContract: true,
        },
        'tldRegistrar:a1:buy': {
            address: 'KT1JRXzmrd3t5wNoFvJxHHmb2yyrbktiAGFf',
        },
        'tldRegistrar:a1:renew': {
            address: 'KT1RSYX2FwpxTbMVQ5HtxRTheJZXNL4mHLVG',
        },
        'tldRegistrar:a1:bid': {
            address: 'KT1TbJQyWTrc8ew9L4ueZNHmoaMzr7S2mu89',
        },
        'tldRegistrar:a1:withdraw': {
            address: 'KT1JjhSLNnyRqgqx89q89HnJrBR1FbZtoX1G',
        },
        'tldRegistrar:a1:settle': {
            address: 'KT1TjmAdyUsyPgRiaWrhrJoEmw3Nx12qQARz',
        },
        'tldRegistrar:a2:commit': {
            address: 'KT1AGWjm71RDyaSSL96TQRaXLcUmSLodJVoN',
        },
        'tldRegistrar:a2': {
            address: 'KT1AGWjm71RDyaSSL96TQRaXLcUmSLodJVoN',
            resolveProxyContract: true,
        },
        'tldRegistrar:a2:buy': {
            address: 'KT1FjZu2QVPxfNm44QkBp31JDP4BXufn27ZV',
        },
        'tldRegistrar:a2:renew': {
            address: 'KT1LxWjBFnEi2Suh8d3JkMB6wrfQ1xyJSQ6R',
        },
        'tldRegistrar:a2:bid': {
            address: 'KT1Euz2CRorGecMD7uS3San9Rws63iEvS73A',
        },
        'tldRegistrar:a2:withdraw': {
            address: 'KT1HiKPUbse3RT6aJAVFS3iSmGwerDy7TCf2',
        },
        'tldRegistrar:a2:settle': {
            address: 'KT1WZ3nikUaSnJTpmg5ZXsy7zwURz9DyUQHF',
        },
        'tldRegistrar:a3:commit': {
            address: 'KT1T1e9NEata4UkTKFJSHM7PFjQpSqrdAXr1',
        },
        'tldRegistrar:a3': {
            address: 'KT1T1e9NEata4UkTKFJSHM7PFjQpSqrdAXr1',
            resolveProxyContract: true,
        },
        'tldRegistrar:a3:buy': {
            address: 'KT1Vq1gBgYzeNvnXaBGdSr3rGcTHadgjYmxp',
        },
        'tldRegistrar:a3:renew': {
            address: 'KT1GtKx25YjxXH7wctf7k6tixguCCJmjRBfc',
        },
        'tldRegistrar:a3:bid': {
            address: 'KT1XYPqEYUgvEwFNHbfuicGsTsAAP8EJ3z9y',
        },
        'tldRegistrar:a3:withdraw': {
            address: 'KT1XSMANsKhTjPyYEpxpMhetgiybPVvRLRWz',
        },
        'tldRegistrar:a3:settle': {
            address: 'KT1QETrskBTdui2KM439ZJMxrmNQ3jgJtvW5',
        },
        oracleRegistrar: {
            address: 'KT1SqTkk584j7Wia3uJLePHikxerJz3gRDXh',
        },
    },
};
